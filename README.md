![输入图片说明](1.jpg)
### 前言
 **经过我们的不懈努力，咱们的S2B2C终于要上线啦 :exclamation:  :exclamation:  :exclamation:**  这个项目也是咱们有多年研发经验的团队花费了数不清的日夜，在大家的努力下终于可以上线。

首先技术方面我们用的是 **Java17 JDK、skywalking8.12.0、spring boot 2.7、dubbo3.2.4 rpc** 调用，不用担心技术过时， **开发体验好，效率高** ，并且方便后期更新迭代。

功能层面我们有 **O2O、储值、消费返利、同城配送，到店自提、库存、代销** 等及丰富的营销版块、代客下单，随着业务发展去不断拓展模块。并且我们可以应用到多重不同的商业模式，及多业务模式可同时运行在一套系统上，让您花一分钱买到两分货。

我们拥有行业领先的技术架构，轻松实现 **千万级并发** ，本项目的目标是打造一个 **功能完整、易于维护** 的微服务电商中台，以满足用户的需求，我们致力于提高产品的质量和性能。

|交流群：Smart shop商城   |交流群：社区团购   |商业咨询   |
|---|---|---|
|![输入图片说明](%E5%95%86%E5%9F%8E%E4%BA%A4%E6%B5%81%E7%BE%A4.png)|![输入图片说明](%E7%A4%BE%E5%8C%BA%E5%9B%A2%E8%B4%AD%E4%BA%A4%E6%B5%81%E7%BE%A4.png)|![输入图片说明](%E5%BE%AE%E4%BF%A1.jpg)|
|QQ群号：458320504   |QQ群号：556731103   |VX号：18158554030   |
<h4>✨项目地址</h4>
<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;点击链接，你将发现更多的惊喜！</h5>
<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;官网
<a  href="https://www.bgniao.cn/" target="_blank">https://www.bgniao.cn</a>
</h5>
<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;平台端&nbsp;&nbsp;
<a  href="https://pro.bgniao.cn/platform/#/" target="_blank">https://pro.bgniao.cn/platform/</a>
</h5>
<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;商家端&nbsp;&nbsp;
<a  href="https://pro.bgniao.cn/shop/#/" target="_blank">https://pro.bgniao.cn/shop/</a>
</h5>
<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;供应商端&nbsp;&nbsp;
<a  href="https://pro.bgniao.cn/supplier/#/sign" target="_blank">https://pro.bgniao.cn/supplier/</a>
</h5>
</h5>
<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;H5端&nbsp;&nbsp;
<a  href="https://pro.bgniao.cn/h5/#/" target="_blank">https://pro.bgniao.cn/h5/#/</a>
</h5>
<h5>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;由于账号密码不定期更新，为了方便访问请联系客服，还有O2O丶S2B2C丶B2C丶B2B等更多商业模式等您咨询👇

### 项目介绍
Smart Shop 是一款基于 **Spring Cloud+MybatisPlus+XXL-JOB+redis+Vue** 的前后端分离、分布式、微服务架构的 Java 商城系统，采用稳定框架开发及优化核心，减少依赖，具备出色的执行效率，扩展性、稳定性高，APP端/小程序端/H5端三端合一数据打通，Smart Shop将持续收集大家反馈不断更新迭代，并经过了生产环境**反复线上论证**和**大量真实用户数据使用**的考验。

### 荣誉资质
| ![输入图片说明](https://images.gitee.com/uploads/images/2021/0907/162352_817a9646_8533008.png "商城软著_20210907161716.png") | ![输入图片说明](https://images.gitee.com/uploads/images/2021/0814/104042_f5d3ec3b_8533008.png "布谷鸟SaaS平台.png") | ![输入图片说明](https://images.gitee.com/uploads/images/2021/0814/104233_f71a9b70_8533008.png "外观专利.png") |
| ---------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------- |

### 技术架构
#### 运行环境（必备）
- CentOS 7.0+
- Nginx 1.10+
- MySQL 8.0+

#### 技术选型
|                      |                   |                                          |
| -------------------- | ----------------- | ---------------------------------------- |
| 技术                   | 说明                | 官网                                       |
| Spring Cloud         | 微服务框架             | https://spring.io/projects/spring-cloud  |
| Spring Cloud Alibaba | 微服务框架             | https://github.com/alibaba/spring-cloud-alibaba |
| Spring Boot          | 容器+MVC 框架         | https://spring.io/projects/spring-boot   |
| Swagger              | 文档生成工具            | https://swagger.io/                      |
| MyBatis-Plus         | 数据层代码生成           | http://www.mybatis.org/generator/index.html |
| RabbitMq             | 消息队列              | https://www.rabbitmq.com/                |
| Redis                | 分布式缓存             | https://redis.io/                        |
| Druid                | 数据库连接池            | https://github.com/alibaba/druid         |
| OSS                  | 对象存储              | https://github.com/aliyun/aliyun-oss-java-sdk |
| JWT                  | JWT 登录支持          | https://github.com/jwtk/jjwt             |
| Lombok               | 简化对象封装工具          | https://github.com/rzwitserloot/lombok   |
| Jenkins              | 自动化部署工具           | https://github.com/jenkinsci/jenkins     |
| Docker               | 应用容器引擎            | https://www.docker.com/                  |
| element              | 组件库               | https://element.eleme.cn/#/zh-CN         |
| Vue.js               | 渐进式 JavaScript 框架 | https://cn.vuejs.org/                    |
| Vue-router           | 前端路由              | https://router.vuejs.org/zh/             |
| vuex                 | 状态管理              | https://vuex.vuejs.org/zh/               |
| modeuse-core         | 自主开发 UI 组件        | --                                       |
| XXL-JOB              | 分布式任务调度平台         | https://www.xuxueli.com/xxl-job/         |
| TypeScript           | JavaScript 超集     | https://www.tslang.cn/                   |
| eslint               | 代码质量控制            | https://eslint.org/                      |
| hook                 | 代码质量控制            |                                          |
| uniapp               | 小程序模板             | https://uniapp.dcloud.io/README          |
| Sonarqube            | 代码质量控制            | https://www.sonarqube.org/               |
| --------             | -------           | -----------------------------------技术    |

#### 业务架构
![](https://images.gitee.com/uploads/images/2021/0706/161344_d0a423b6_8533008.png "Smart Shop系统架构图.png") 
- gruul-platform-open 平台模块
- gruul-account-open 账户模块
- gruul-gateway-open 网关
- gruul-order-open 订单模块
- gruul-goods-open 商品模块
- gruul-payment-open 支付模块
- gruul-oss-openOSS 模块
- gruul-afs-open 售后模块
- gruul-shops-open 店铺模块
- gruul-common-open 公共模块
- gruul-logistics-open 物流模块
- gruul-sms-open 短信模块

#### 编码规范
- 规范方式：后端严格遵守阿里编码规约，前端使用 eslint；
- 命名统一：简介最大程度上达到了见名知意；
- 分包明确：层级分明可快速定位到代码位置；
- 注释完整：描述性高大量减少了开发人员的代码阅读工作量；
- 工具规范：使用统一 jar 包避免出现内容冲突；
- 代码整洁：可读性、维护性更高。

  **代码位置**

  商家端 :https://gitee.com/qisange/basemall/tree/master/group-mall-admin-source

  小程序端 :https://gitee.com/qisange/basemall/tree/master/group-shop-uniapp-wx-Source

  后端 :https://gitee.com/qisange/basemall/tree/master/gruul

  数据库 ：https://gitee.com/qisange/basemall/tree/master/gruul/init-default-sql

💝 **如果觉得我们的项目对你有帮助，可点击右上角 Watch、Star 项目，获取项目第一时间更新，欢迎提交 Issues 和 PR 项目，如需需求文档、流程图联系技术获取！**
## [更新详细说明](https://www.bgniao.cn/notice) https://www.bgniao.cn/
