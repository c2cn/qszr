/*
 * @description: 环境变量名 代码提示作用
 * @Author: vikingShip
 * @Date: 2022-05-22 20:41:30
 * @LastEditors: vikingShip
 * @LastEditTime: 2022-05-22 20:41:42
 */
 interface ImportMetaEnv {
    VITE_BASE_MAIN_PATH:string
    VITE_BASE_IMAGE_URL: string
    VITE_BASE_URL:string
    VITE_LOCAL_STORAGE_KEY_PREFIX:string
    VITE_REQUEST_TIME_OUT:string
}