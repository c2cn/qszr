# SmartShop电商中台 S2B2C供应链商城 供货商平台

#### Description
🥇🥇🥇启山智软商城基于Spring Boot+Spring Cloud电商中台系统，支持APP端商城、H5端商城、小程序端商城、PC端商城。支持业务模式包括：S2B2C商城、B2B2C商城、O2O商城、B2C商城、多商户商城、分销商城、单商户商城等。S2B2C商城是供应链商城，它是集合了供货商的渠道平台。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
