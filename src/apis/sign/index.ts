/*
 * @description:
 * @Author: vikingShip
 * @Date: 2022-03-22 13:13:59
 * @LastEditors: vikingShip
 * @LastEditTime: 2022-04-12 17:00:06
 */
import { post } from "../http";

export const signByUser = (data:any) => {
  return post({
    showLoading: true,
    url: "/gruul-mall-uaa/uaa/auth/oauth/token",
    data
  });
};
